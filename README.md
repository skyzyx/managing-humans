# Being Geek ([cache](https://www.evernote.com/pub/skyzyx/beinggeek))
* [Being Geek](http://beinggeek.com) — Book
* [Stables and Volatiles](http://randsinrepose.com/archives/stables-and-volatiles/) — Take crazy risks
* [Busy is an Addiction](http://randsinrepose.com/archives/busy-is-an-addiction/) — The dubious value of being busy
* [An Ideal Conversation](http://randsinrepose.com/archives/an-ideal-conversation/) — Predicting the social weather
* [The Song of the Introvert](http://randsinrepose.com/archives/the-song-of-the-introvert/) — You are new data
* [Infinite State](http://randsinrepose.com/archives/infinite-state/) — The absolute best kind of state out there
* [An Introduction to You](http://randsinrepose.com/archives/an-introduction-to-you/) — You are a complex concept
* [The Process Myth](http://randsinrepose.com/archives/the-process-myth/) — Insist on understanding
* [Free Electron](http://randsinrepose.com/archives/free-electron/) — The single most productive engineer
* [We’re Doomed](http://randsinrepose.com/archives/were-doomed/) — Software development inflection points

# Managing Humans ([cache](https://www.evernote.com/pub/skyzyx/managinghumans))
* [Managing Humans](http://managinghumans.com) — Book
* [The Rands Test](http://randsinrepose.com/archives/the-rands-test/) — Start here
* [A Toxic Paradox](http://randsinrepose.com/archives/a-toxic-paradox/) — Real change is hard
* [A Disclosure](http://randsinrepose.com/archives/a-disclosure/) — Trust so you can scale
* [The Larry Test](http://randsinrepose.com/archives/the-larry-test/) — Are we done?
* [Subtlety, Subterfuge, and Silence](http://randsinrepose.com/archives/subtlety-subterfuge-and-silence/) — In silence, you can assess
* [Don’t Be A Prick](http://randsinrepose.com/archives/dont-be-a-prick/) — A management style that scales
* [Chaotic Beautiful Snowflakes](http://randsinrepose.com/archives/chaotic-beautiful-snowflakes/) — And that’s just you
* [Entropy Crushers](http://randsinrepose.com/archives/entropy-crushers/) — Do you want to be an engineer?
* [The Wolf](http://randsinrepose.com/archives/the-wolf/) — Dictating their own terms
* [Shields Down](http://randsinrepose.com/archives/shields-down/) — Happy people don’t leave jobs they love
* [Why we don’t speak up at work](https://blog.knowyourcompany.com/why-we-don-t-speak-up-at-work-94abeb1c8f36#.jtmc1os41)

# 1:1s ([cache](https://www.evernote.com/pub/skyzyx/11s))
* [The Update, The Vent, and The Disaster](http://randsinrepose.com/archives/the-update-the-vent-and-the-disaster/) — A distinct lack of drama
* [Friction, Pain, and Failure](http://randsinrepose.com/archives/friction-pain-and-failure/) — Then consequences will become obvious
* [You’re Not Listening](http://randsinrepose.com/archives/youre-not-listening/) — The world knows a lot more than you
* [Lost in Translation](http://randsinrepose.com/archives/lost-in-translation/) — Learn schmearn. I'm a genius.
* [Triggers](http://randsinrepose.com/archives/triggers/) — Blind, piercing rage
* [No Surprises](http://randsinrepose.com/archives/no-surprises/) — What you need to do next

# Culture, Teams, Mentorship ([cache](https://www.evernote.com/pub/skyzyx/cultureteamsmentorship))
* [Someone is Coming to Eat You](http://randsinrepose.com/archives/someone-is-coming-to-eat-you/) — Faster than you
* [I Have a Hunch](http://randsinrepose.com/archives/i-have-a-hunch/) — We miss engineering
* [Seven Plus or Minus Three](http://randsinrepose.com/archives/seven-plus-or-minus-three/) — Avoidable high fructose random shit minutes
* [The Old Guard](http://randsinrepose.com/archives/the-old-guard/) — Comfortable chaos into legitimate chaos
* [The Culture Chart](http://randsinrepose.com/archives/the-culture-chart/) — We must not ship crap
* [Secret Titles](http://randsinrepose.com/archives/secret-titles/) — Osmosis hallway guy
* [The Mentor Manifesto](http://www.davidgcohen.com/2011/08/28/the-mentor-manifesto/)
* [Microaggression and Management](https://web.archive.org/web/20140405220327/https://medium.com/about-work/65d4740f7a2f)
* [Some notes on culture](http://ataussig.com/post/32402969166/some-notes-on-culture)
* [What Google Taught Me About Scaling Engineering Teams](http://www.theeffectiveengineer.com/blog/what-i-learned-from-googles-engineering-culture)
* [Why Managers Are So Bad at Recognizing Good Ideas](http://www.theatlantic.com/business/archive/2016/06/adam-grant-aspen/489056/)

# Hiring
* [A Glimpse and a Hook](http://randsinrepose.com/archives/a-glimpse-and-a-hook/) — You've got 30 seconds
* [A Brief Glimpse](http://randsinrepose.com/archives/a-brief-glimpse/) — A sea of mediocre resumes
* [The Sanity Check](http://randsinrepose.com/archives/the-sanity-chec/) — Stalk your future job
* [Ninety Days](http://randsinrepose.com/archives/ninety-days/) — Say something really stupid
* [The Great (Incorrect) Disappointment](http://randsinrepose.com/archives/the-great-incorrect-disappointment/) — People don’t believe what they have not seen
* [Wanted](http://randsinrepose.com/archives/wanted/) — Hire for your career
* [The Diving Save](http://randsinrepose.com/archives/the-diving-save/) — Do you really want to do this?
* [This is why you never end up hiring good developers](http://qz.com/258066/this-is-why-you-dont-hire-good-developers/)

# Meetings
* [How to Run a Meeting](http://randsinrepose.com/archives/how-to-run-a-meeting/) — Rules so people know when to talk
* [Bits, Features, and Truth](http://randsinrepose.com/archives/bits-features-and-truth/) — Gives those forces a proper name
* [The Twinge](http://randsinrepose.com/archives/the-twinge/) — Amorphous moments of clarity
* [The Screw-Me Scenario](http://randsinrepose.com/archives/the-screwme-scenario/) — Paranoia is a lot of work
* [Agenda Detection](http://randsinrepose.com/archives/agenda-detection/) — Players and pawns. Pros and cons.

# Managing Crises
* [The Crisis and the Creative](http://randsinrepose.com/archives/the-crisis-and-the-creative/) — Investments in an uncertain future
* [A Deep Breath](http://randsinrepose.com/archives/a-deep-breath/) — A deliberate moment of consideration
